# quick-emojis

A quick script for copying emojis to the clipboard using dmenu-style program.

It's a small script made for personnal use but I figured I could share it because I found it quite hard to get a good unicode character list.

## source

The emoji list was created by parsing https://unicode.org/Public/emoji/15.1/emoji-test.txt

## usage

First generate the emojis.txt file :

```sh
curl https://unicode.org/Public/emoji/15.1/emoji-test.txt | awk -f parse.awk > emojis.txt
  
```

Example usage :

```sh
dmenu < emojis.txt | cut -f4 -d' ' | xclip
```

